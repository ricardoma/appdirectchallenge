var express = require('express')
    , app = express()
    , twitterClient = require('./twitterClient')

app.use(express.static(__dirname + '/public'));

// Route to load tweets for a twitter user
app.get('/api/tweets/:screen_name', twitterClient.tweets);

// Route to load usernames from twitter
app.get('/api/users', twitterClient.users);

// For any route that doesn't point ot the API send index.html which will start angular
app.get('*', function (req, res) {
    res.sendFile(__dirname + '/public/views/index.html');
});

var server = app.listen((process.env.PORT || 3000), function () {

    var host = server.address().address;
    var port = server.address().port;

    console.log('Example app listening at http://%s:%s', host, port);

});