(function() {
    'use strict';

    angular.module('app')
        .directive('header', function(SettingsService) {
            return {
                restrict: 'E'
                , replace: true
                , scope: {}
                , templateUrl: '/views/header.html'
                , link: linkFunction
            };

            function linkFunction(scope, element, attrs) {

                // Expose service
                scope.settingsService = SettingsService;

                // Activate/Deactivate drag and dropping
                scope.toggleDragAndDrop = function() {
                    SettingsService.toggleDragAndDrop();
                }
            }


        });

})();