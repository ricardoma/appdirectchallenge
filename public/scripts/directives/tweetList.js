(function() {
    'use strict';

    angular.module('app')
        .directive('tweetList', function(TweetsService) {
            return {
                restrict: 'E'
                , replace: true
                , scope: {user: '='}
                , templateUrl: '/views/tweetList.html'
                , link: linkFunction
            };

            function linkFunction(scope, element, attrs) {
                var max_id;

                // Indicates if we're loading tweets
                scope.loading = true;

                // Tweets to be displayed
                scope.tweets = [];

                // Call getTweetsForScreenName, and attach handlers to the returned promise
                function loadTweets() {
                    TweetsService.getTweetsForScreenName(scope.user.screen_name, max_id, scope.user.numberOfTweets)
                        .success(function(data, status, headers, config) {

                            // Append loaded tweets to the scope
                            Array.prototype.push.apply(scope.tweets, data);

                            // Save max_id to load the next batch
                            max_id = data[data.length - 1].id_str;

                            // Indicate that we finished loading the tweets
                            scope.loading = false;
                        }).error(function(data, status, headers, config) {
                            console.log('Error when loading tweets');
                        });
                }

                // Called when the user reaches the bottom of the tweets list, therefore load next batch
                scope.loadMore = function() {
                    if (!scope.loading) {
                        loadTweets();
                        scope.loading = true;
                    }
                };

                // Load initial batch
                loadTweets();
            }


        });

})();