(function() {
    'use strict';

    angular.module('app')
        .directive('columnSettings', function($compile, SettingsService) {
            return {
                restrict: 'A'
                , link: linkFunction
            };

            function linkFunction(scope, element, attrs) {
                scope.form = {
                    // Number of tweets loaded with each batch
                    tweetsPerPage: scope.user.numberOfTweets,
                    // Delete Column
                    deleteColumn: function() {
                        SettingsService.delete(scope.user);
                    },
                    // Save new number of tweets per batch value
                    save: function() {
                        scope.user.numberOfTweets = scope.form.tweetsPerPage;
                        SettingsService.saveSettings();
                        $(element).popover('hide');
                    }
                };

                var content = [
                        '<form>',
                            '<div class="form-group" width="500px">',
                                '<label for="exampleInputEmail1">Tweets per page: {{form.tweetsPerPage}}</label>',
                                '<input ng-model="form.tweetsPerPage" type="range" name="points" min="15" max="50">',
                            '</div>',
                            '<button class="btn btn-default btn-sm" ng-click="form.save()" style="float:left">Save</button>',
                            '<button class="btn btn-danger btn-sm" ng-click="form.deleteColumn()" style="float:right">Delete</button>',
                        '</form>'
                ].join('');

                // Compile template with the scope
                content = $compile(content)(scope);

                // Initialize popover
                $(element).popover({
                    title: 'Settings',
                    trigger: 'click',
                    html: true,
                    content: content,
                    placement: 'left'
                });
            }


        });

})();