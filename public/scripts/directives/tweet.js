(function() {
    'use strict';

    angular.module('app')
        .directive('tweet', function($sce) {
            return {
                restrict: 'E'
                , replace: true
                , scope: {tweet: '='}
                , templateUrl: '/views/tweet.html'
                , link: linkFunction
            };

            function linkFunction(scope, element, attrs) {
                // Overwrite tweet in case it's a retweet
                if (!!scope.tweet.retweeted_status) {
                    scope.tweet = scope.tweet.retweeted_status;
                }

                // Parse tweet text with Autolinker, to make links, hastags and users links
                scope.tweet.text = $sce.trustAsHtml(Autolinker.link(scope.tweet.text));

                // Use moment.calendar() to create beautiful date
                scope.tweet.created_at = moment(new Date(scope.tweet.created_at)).calendar();
            }

        });

})();