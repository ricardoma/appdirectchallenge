(function () {
    'use strict';

    // This directive calls the whenScrolled function when the scroll reaches the bottom of the container
    angular.module('app').directive('whenScrolled', function () {
        return {
            restrict: 'A'
            , scope: {triggerOffset: "=", whenScrollEnabled: "=?", whenScrolled: "&"} //whenScrollEnabled is optional if set to false then whenScroll function wont be called
            , link: linkFunction
        };

        function linkFunction(scope, elm, attr) {
            var raw = elm[0];

            // At which point should the event be fired
            var triggerOffsetPercentage = (parseFloat(scope.triggerOffset) || 0);

            // Ability to disable
            if (scope.whenScrollEnabled === undefined) {
                scope.whenScrollEnabled = true
            }

            elm.bind('scroll', function () {
                if (scope.whenScrollEnabled) {
                    var offset = raw.scrollHeight * triggerOffsetPercentage;

                    // If the scroll passed the indicated offset call the whenScrolled function
                    if (raw.scrollTop + raw.offsetHeight + offset >= raw.scrollHeight) {
                        scope.$apply(scope.whenScrolled);
                    }
                }
            });

        }

    });
})();