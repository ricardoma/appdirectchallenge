(function() {
    'use strict';

    angular.module('app')
        .directive('skinSettings', function($compile, SettingsService) {
            return {
                restrict: 'A'
                , link: linkFunction
            };

            function linkFunction(scope, element, attrs) {
                var content;

                // Skin colors
                scope.colors = [
                    {class:'blue-sheet', 'backgroundColor': 'rgb(14, 116, 175)'},
                    {class:'yellow-sheet', 'backgroundColor': 'rgb(228, 226, 65);'},
                    {class:'green-sheet', 'backgroundColor': 'rgb(86, 236, 88);'},
                    {class:'pink-sheet', 'backgroundColor': 'rgb(249, 87, 234)'},
                    {class:'red-sheet', 'backgroundColor': ' rgb(249, 87, 87)'}
                ];

                // Change skin by applying className to the body
                scope.setSkin = function(className) {
                    $('body')[0].className = className;
                    SettingsService.setSkinClass(className);
                };

                // Color options template
                content = [
                    '<div width="300px">',
                    '<div ng-click="setSkin(color.class)" ng-repeat="color in colors" style="height: 50px; cursor: pointer; background-color: {{color.backgroundColor}}"></div>',
                    '</div>'
                ].join('');


                // Compilte it with the scope
                content = $compile(content)(scope);

                // Initialize popover
                $(element).popover({
                    title: 'Skins',
                    trigger: 'click',
                    html: true,
                    content: content,
                    placement: 'bottom'
                });

            }


        });

})();