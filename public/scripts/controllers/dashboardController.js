angular.module('app')
    .controller('dashboardController', function($scope, SettingsService){

        // Options for the sortable directive
        $scope.sortableOptions = {
            containment: '.lists-container'
        };

        // Expose settings to the scope
        $scope.settingsService = SettingsService;

        // Watch when new users accounts are added, to create columns
        $scope.$watchCollection(
            function() {return SettingsService.getUsers()},
            function(newVal) {
                $scope.users = newVal;
                SettingsService.saveSettings();
            }
        )
    });