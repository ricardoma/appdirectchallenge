(function() {
    'use strict';

    angular.module('app')
        .controller('addUserController', function($scope, UsersService, SettingsService){
            $scope.model = {user: '', loading: false};

            // Will contain users returned from the API
            $scope.users = [];

            $scope.$watch('model.user', function(newVal) {

                // If newVal exists (differentt than '')
                if (newVal) {
                    $scope.model.loading = true;

                    // Ask for users, attach handlers to the promise
                    UsersService.getUsers(newVal)
                        .success(function(data, status, headers, config) {

                            // If succeed attach users to the scope
                            $scope.users = data;

                            // Indicate we finished loading
                            $scope.model.loading = false;
                        }).error(function(data, status, headers, config) {
                            console.log('Couldnt load users');
                        });
                } else {

                    // Since newVal was '', reset the users array
                    $scope.model.loading = false;
                    $scope.users = [];
                }
            });

            $scope.addUser = function(user) {
                SettingsService.addUser(user.screen_name);
                //TODO: Remove this dom interaction from the controller
                $('#addNewUserModal').modal('hide');
            };


        });
})();