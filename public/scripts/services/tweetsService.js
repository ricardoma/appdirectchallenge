(function() {
    'use strict';

    angular.module('app')
        .factory('TweetsService', function($http) {
            var service = {};

            service.getTweetsForScreenName = function(screenName, max_id, count) {
                var config = {
                    method: 'GET',
                    url: '/api/tweets/' + screenName,
                    params: {}
                };

                // Set number of tweets to be loaded
                if (count) config.params.count = count;
                // Max_id specifies next batch of tweets
                if (max_id) config.params.max_id = max_id;

                return $http(config);
            };

            return service;
        });

})();
