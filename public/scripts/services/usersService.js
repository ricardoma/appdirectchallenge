(function() {
    'use strict';

    angular.module('app')
        .factory('UsersService', function($http) {
            var service = {};

            service.getUsers = function(q) {
                return $http.get('/api/users?q=' + q);
            };

            return service;
        });

})();
