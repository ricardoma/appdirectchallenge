(function() {
    'use strict';

    angular.module('app')
        .factory('SettingsService', function() {
            var service = {};

            // Read settings from localStorage
            service.settings = JSON.parse(localStorage.getItem('settings'));

            // If settings don't exist, create them with default settings
            if (!service.settings) {
                service.settings = {
                    users: [
                        {screen_name: 'TechCrunch', numberOfTweets: 20},
                        {screen_name: 'AppDirect', numberOfTweets: 20},
                        {screen_name: 'LaughingSquid', numberOfTweets: 20}
                    ],
                    isDragAndDropActive: false,
                    skinClass: 'blue-sheet'
                };
                saveSettings();
            }

            function addUser(screenName) {
                service.settings.users.push({screen_name: screenName, dateRange: [], numberOfTweets: 20});
                saveSettings();
            }

            function saveSettings() {
                localStorage.setItem('settings', angular.toJson(service.settings));
            }

            service.setSkinClass = function(className){
                service.settings.skinClass = className;
                saveSettings();
            };

            service.getUsers = function() {
                return service.settings.users;
            };

            service.addUser = function(screenName) {
                addUser(screenName);
            };

            service.saveSettings = function() {
                saveSettings();
            };

            service.delete = function(user) {
                var index = service.settings.users.indexOf(user);
                service.settings.users.splice(index, 1);
            };

            service.toggleDragAndDrop = function() {
                service.settings.isDragAndDropActive = !service.settings.isDragAndDropActive;
                saveSettings();
            };

            return service;
        });

})();
