(function() {
    'use strict';

    angular.module('app', ['ngRoute', 'ui.sortable', 'ngSanitize'])
        .config(function($locationProvider, $routeProvider){
            $routeProvider
                .when('/', {
                    templateUrl: 'views/dashboard.html',
                    controller: 'dashboardController'
                });

            $locationProvider.html5Mode(true);
        })
        .run(function(SettingsService) {
            // As soon as the app is running assign skin class to the body
            $('body')[0].className = SettingsService.settings.skinClass || 'blue-sheet';
        });
})();