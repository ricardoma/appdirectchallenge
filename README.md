# AppDirect twitter dashboard challenge

Hi AppDirect Team,

To build the dashboard I decided to use angular, bootstrap and express, with the goal of iterating quickly.

## Server side

The server side is extremely lightweight, I had to create it since the twitter API doesn't allow CORS calls.
It's an express application which exposes two routes:

* /api/tweets/:screen_name receives the screen_name of a user and returns the timeline.
* /api/users this route receives a parameter called 'q' and returns users that match that criteria in their name.
* The application also serves the public files, which include the views, bower components and the angular app.

## Front end

The front end is an angular application. All the views are stored under public/views, and the application javascript files are under /public/scripts.
Here is a brief explanation of each component:

## Controllers
  * addUserController: This controller is in charge of giving the user the ability to add new columns.
  * dashBoardController: This is the main controller it exposes the number of columns and creates the options for the drag and drop directive.

## Directives:
  * columnSettings: Displays a popover so the user can delete the column or change the number of tweets loaded with each batch.
  * header: handles the logic in the header of the app
  * skinSettings: displays popover with different skins so the user can pick one
  * tweet: Parses the content of the tweet, check if it's a retweet and formats the date
  * tweetList: Loads the tweets for the column
  * whenScrolled: used as a helper to call the load more tweets function when the scroll reaches the end

## Services:
  * settingsService: Contains the state for the settings, columns order, tweets to be loaded per batch and skin
  * tweetsService: Makes requests to the timeline API endpoint and returns promises
  * usersService: Makes requests to the users API endpoint and returns promises

## Other:

  * App.js: Starts the angular app

## Features:

* Display many timelines in form of columns
* Drag and drop to reorder the columns
* Pick skin
* Choose number of tweets loaded per batch
* Add new columns for any user on twitter
* It wasn't possible to add a date range for the tweets since twitter's timeline API endpoint doesn't offer that option

## Installation:
  * The application is live at: https://appdirectricardo.herokuapp.com/
  * To run locally, clone this repository, and then run the following commands:
    * bower install
    * npm install
    * npm start
    * visit http://localhost:3000/






