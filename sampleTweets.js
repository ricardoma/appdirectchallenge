module.exports = [
    {
        "created_at": "Tue May 19 17:02:51 +0000 2015",
        "id": 600708204389998592,
        "id_str": "600708204389998592",
        "text": "Voltus Gives The 12-Inch MacBook A Battery Boost, Two New USB Ports http:\/\/t.co\/8yNvvfCxsP by @etherington http:\/\/t.co\/ihWskim9ab",
        "source": "\u003ca href=\"http:\/\/10up.com\" rel=\"nofollow\"\u003e10up Publish Tweet\u003c\/a\u003e",
        "truncated": false,
        "in_reply_to_status_id": null,
        "in_reply_to_status_id_str": null,
        "in_reply_to_user_id": null,
        "in_reply_to_user_id_str": null,
        "in_reply_to_screen_name": null,
        "user": {
            "id": 816653,
            "id_str": "816653",
            "name": "TechCrunch",
            "screen_name": "TechCrunch",
            "location": "San Francisco, CA",
            "description": "Breaking technology news, analysis, and opinions from TechCrunch. The number one guide for all things tech.",
            "url": "http:\/\/t.co\/b5Oyx12qGG",
            "entities": {
                "url": {
                    "urls": [
                        {
                            "url": "http:\/\/t.co\/b5Oyx12qGG",
                            "expanded_url": "http:\/\/techcrunch.com",
                            "display_url": "techcrunch.com",
                            "indices": [0, 22]
                        }
                    ]
                },
                "description": {
                    "urls": []
                }
            },
            "protected": false,
            "followers_count": 5104212,
            "friends_count": 683,
            "listed_count": 97340,
            "created_at": "Wed Mar 07 01:27:09 +0000 2007",
            "favourites_count": 593,
            "utc_offset": -25200,
            "time_zone": "Pacific Time (US & Canada)",
            "geo_enabled": true,
            "verified": true,
            "statuses_count": 99688,
            "lang": "en",
            "contributors_enabled": false,
            "is_translator": false,
            "is_translation_enabled": true,
            "profile_background_color": "149500",
            "profile_background_image_url": "http:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_image_url_https": "https:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_tile": false,
            "profile_image_url": "http:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_image_url_https": "https:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_banner_url": "https:\/\/pbs.twimg.com\/profile_banners\/816653\/1431096993",
            "profile_link_color": "097000",
            "profile_sidebar_border_color": "FFFFFF",
            "profile_sidebar_fill_color": "DDFFCC",
            "profile_text_color": "222222",
            "profile_use_background_image": true,
            "default_profile": false,
            "default_profile_image": false,
            "following": true,
            "follow_request_sent": false,
            "notifications": false
        },
        "geo": null,
        "coordinates": null,
        "place": null,
        "contributors": null,
        "retweet_count": 31,
        "favorite_count": 36,
        "entities": {
            "hashtags": [],
            "symbols": [],
            "user_mentions": [
                {
                    "screen_name": "etherington",
                    "name": "Darrell Etherington",
                    "id": 15425183,
                    "id_str": "15425183",
                    "indices": [94, 106]
                }
            ],
            "urls": [
                {
                    "url": "http:\/\/t.co\/8yNvvfCxsP",
                    "expanded_url": "http:\/\/tcrn.ch\/1F02osA",
                    "display_url": "tcrn.ch\/1F02osA",
                    "indices": [68, 90]
                }
            ],
            "media": [
                {
                    "id": 600708203005890560,
                    "id_str": "600708203005890560",
                    "indices": [107, 129],
                    "media_url": "http:\/\/pbs.twimg.com\/media\/CFYk7Y3UUAAo6Ec.jpg",
                    "media_url_https": "https:\/\/pbs.twimg.com\/media\/CFYk7Y3UUAAo6Ec.jpg",
                    "url": "http:\/\/t.co\/ihWskim9ab",
                    "display_url": "pic.twitter.com\/ihWskim9ab",
                    "expanded_url": "http:\/\/twitter.com\/TechCrunch\/status\/600708204389998592\/photo\/1",
                    "type": "photo",
                    "sizes": {
                        "small": {
                            "w": 340,
                            "h": 226,
                            "resize": "fit"
                        },
                        "thumb": {
                            "w": 150,
                            "h": 150,
                            "resize": "crop"
                        },
                        "medium": {
                            "w": 600,
                            "h": 400,
                            "resize": "fit"
                        },
                        "large": {
                            "w": 1024,
                            "h": 682,
                            "resize": "fit"
                        }
                    }
                }
            ]
        },
        "extended_entities": {
            "media": [
                {
                    "id": 600708203005890560,
                    "id_str": "600708203005890560",
                    "indices": [107, 129],
                    "media_url": "http:\/\/pbs.twimg.com\/media\/CFYk7Y3UUAAo6Ec.jpg",
                    "media_url_https": "https:\/\/pbs.twimg.com\/media\/CFYk7Y3UUAAo6Ec.jpg",
                    "url": "http:\/\/t.co\/ihWskim9ab",
                    "display_url": "pic.twitter.com\/ihWskim9ab",
                    "expanded_url": "http:\/\/twitter.com\/TechCrunch\/status\/600708204389998592\/photo\/1",
                    "type": "photo",
                    "sizes": {
                        "small": {
                            "w": 340,
                            "h": 226,
                            "resize": "fit"
                        },
                        "thumb": {
                            "w": 150,
                            "h": 150,
                            "resize": "crop"
                        },
                        "medium": {
                            "w": 600,
                            "h": 400,
                            "resize": "fit"
                        },
                        "large": {
                            "w": 1024,
                            "h": 682,
                            "resize": "fit"
                        }
                    }
                }
            ]
        },
        "favorited": false,
        "retweeted": false,
        "possibly_sensitive": false,
        "lang": "en"
    },
    {
        "created_at": "Tue May 19 16:59:41 +0000 2015",
        "id": 600707409372442624,
        "id_str": "600707409372442624",
        "text": "Popcorn Time Now Streams Movies To A Browser Making It Scary Easy To Pirate http:\/\/t.co\/j4JbjA8gwk by @mjburnsy",
        "source": "\u003ca href=\"http:\/\/10up.com\" rel=\"nofollow\"\u003e10up Publish Tweet\u003c\/a\u003e",
        "truncated": false,
        "in_reply_to_status_id": null,
        "in_reply_to_status_id_str": null,
        "in_reply_to_user_id": null,
        "in_reply_to_user_id_str": null,
        "in_reply_to_screen_name": null,
        "user": {
            "id": 816653,
            "id_str": "816653",
            "name": "TechCrunch",
            "screen_name": "TechCrunch",
            "location": "San Francisco, CA",
            "description": "Breaking technology news, analysis, and opinions from TechCrunch. The number one guide for all things tech.",
            "url": "http:\/\/t.co\/b5Oyx12qGG",
            "entities": {
                "url": {
                    "urls": [
                        {
                            "url": "http:\/\/t.co\/b5Oyx12qGG",
                            "expanded_url": "http:\/\/techcrunch.com",
                            "display_url": "techcrunch.com",
                            "indices": [0, 22]
                        }
                    ]
                },
                "description": {
                    "urls": []
                }
            },
            "protected": false,
            "followers_count": 5104212,
            "friends_count": 683,
            "listed_count": 97340,
            "created_at": "Wed Mar 07 01:27:09 +0000 2007",
            "favourites_count": 593,
            "utc_offset": -25200,
            "time_zone": "Pacific Time (US & Canada)",
            "geo_enabled": true,
            "verified": true,
            "statuses_count": 99688,
            "lang": "en",
            "contributors_enabled": false,
            "is_translator": false,
            "is_translation_enabled": true,
            "profile_background_color": "149500",
            "profile_background_image_url": "http:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_image_url_https": "https:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_tile": false,
            "profile_image_url": "http:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_image_url_https": "https:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_banner_url": "https:\/\/pbs.twimg.com\/profile_banners\/816653\/1431096993",
            "profile_link_color": "097000",
            "profile_sidebar_border_color": "FFFFFF",
            "profile_sidebar_fill_color": "DDFFCC",
            "profile_text_color": "222222",
            "profile_use_background_image": true,
            "default_profile": false,
            "default_profile_image": false,
            "following": true,
            "follow_request_sent": false,
            "notifications": false
        },
        "geo": null,
        "coordinates": null,
        "place": null,
        "contributors": null,
        "retweet_count": 31,
        "favorite_count": 15,
        "entities": {
            "hashtags": [],
            "symbols": [],
            "user_mentions": [
                {
                    "screen_name": "mjburnsy",
                    "name": "Matt Burns",
                    "id": 17223098,
                    "id_str": "17223098",
                    "indices": [102, 111]
                }
            ],
            "urls": [
                {
                    "url": "http:\/\/t.co\/j4JbjA8gwk",
                    "expanded_url": "http:\/\/tcrn.ch\/1Ba9ER8",
                    "display_url": "tcrn.ch\/1Ba9ER8",
                    "indices": [76, 98]
                }
            ]
        },
        "favorited": false,
        "retweeted": false,
        "possibly_sensitive": false,
        "lang": "en"
    },
    {
        "created_at": "Tue May 19 16:57:43 +0000 2015",
        "id": 600706914461192192,
        "id_str": "600706914461192192",
        "text": "The First Apple Watch OS Update Is Now Available http:\/\/t.co\/fdvR9NOqWu by @etherington",
        "source": "\u003ca href=\"http:\/\/10up.com\" rel=\"nofollow\"\u003e10up Publish Tweet\u003c\/a\u003e",
        "truncated": false,
        "in_reply_to_status_id": null,
        "in_reply_to_status_id_str": null,
        "in_reply_to_user_id": null,
        "in_reply_to_user_id_str": null,
        "in_reply_to_screen_name": null,
        "user": {
            "id": 816653,
            "id_str": "816653",
            "name": "TechCrunch",
            "screen_name": "TechCrunch",
            "location": "San Francisco, CA",
            "description": "Breaking technology news, analysis, and opinions from TechCrunch. The number one guide for all things tech.",
            "url": "http:\/\/t.co\/b5Oyx12qGG",
            "entities": {
                "url": {
                    "urls": [
                        {
                            "url": "http:\/\/t.co\/b5Oyx12qGG",
                            "expanded_url": "http:\/\/techcrunch.com",
                            "display_url": "techcrunch.com",
                            "indices": [0, 22]
                        }
                    ]
                },
                "description": {
                    "urls": []
                }
            },
            "protected": false,
            "followers_count": 5104212,
            "friends_count": 683,
            "listed_count": 97340,
            "created_at": "Wed Mar 07 01:27:09 +0000 2007",
            "favourites_count": 593,
            "utc_offset": -25200,
            "time_zone": "Pacific Time (US & Canada)",
            "geo_enabled": true,
            "verified": true,
            "statuses_count": 99688,
            "lang": "en",
            "contributors_enabled": false,
            "is_translator": false,
            "is_translation_enabled": true,
            "profile_background_color": "149500",
            "profile_background_image_url": "http:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_image_url_https": "https:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_tile": false,
            "profile_image_url": "http:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_image_url_https": "https:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_banner_url": "https:\/\/pbs.twimg.com\/profile_banners\/816653\/1431096993",
            "profile_link_color": "097000",
            "profile_sidebar_border_color": "FFFFFF",
            "profile_sidebar_fill_color": "DDFFCC",
            "profile_text_color": "222222",
            "profile_use_background_image": true,
            "default_profile": false,
            "default_profile_image": false,
            "following": true,
            "follow_request_sent": false,
            "notifications": false
        },
        "geo": null,
        "coordinates": null,
        "place": null,
        "contributors": null,
        "retweet_count": 25,
        "favorite_count": 10,
        "entities": {
            "hashtags": [],
            "symbols": [],
            "user_mentions": [
                {
                    "screen_name": "etherington",
                    "name": "Darrell Etherington",
                    "id": 15425183,
                    "id_str": "15425183",
                    "indices": [75, 87]
                }
            ],
            "urls": [
                {
                    "url": "http:\/\/t.co\/fdvR9NOqWu",
                    "expanded_url": "http:\/\/tcrn.ch\/1GmKR2e",
                    "display_url": "tcrn.ch\/1GmKR2e",
                    "indices": [49, 71]
                }
            ]
        },
        "favorited": false,
        "retweeted": false,
        "possibly_sensitive": false,
        "lang": "en"
    },
    {
        "created_at": "Tue May 19 16:45:43 +0000 2015",
        "id": 600703891961872384,
        "id_str": "600703891961872384",
        "text": "Authy Launches Simplified Two-Factor Authentication http:\/\/t.co\/rx6byugeEs by @fredericl",
        "source": "\u003ca href=\"http:\/\/10up.com\" rel=\"nofollow\"\u003e10up Publish Tweet\u003c\/a\u003e",
        "truncated": false,
        "in_reply_to_status_id": null,
        "in_reply_to_status_id_str": null,
        "in_reply_to_user_id": null,
        "in_reply_to_user_id_str": null,
        "in_reply_to_screen_name": null,
        "user": {
            "id": 816653,
            "id_str": "816653",
            "name": "TechCrunch",
            "screen_name": "TechCrunch",
            "location": "San Francisco, CA",
            "description": "Breaking technology news, analysis, and opinions from TechCrunch. The number one guide for all things tech.",
            "url": "http:\/\/t.co\/b5Oyx12qGG",
            "entities": {
                "url": {
                    "urls": [
                        {
                            "url": "http:\/\/t.co\/b5Oyx12qGG",
                            "expanded_url": "http:\/\/techcrunch.com",
                            "display_url": "techcrunch.com",
                            "indices": [0, 22]
                        }
                    ]
                },
                "description": {
                    "urls": []
                }
            },
            "protected": false,
            "followers_count": 5104212,
            "friends_count": 683,
            "listed_count": 97340,
            "created_at": "Wed Mar 07 01:27:09 +0000 2007",
            "favourites_count": 593,
            "utc_offset": -25200,
            "time_zone": "Pacific Time (US & Canada)",
            "geo_enabled": true,
            "verified": true,
            "statuses_count": 99688,
            "lang": "en",
            "contributors_enabled": false,
            "is_translator": false,
            "is_translation_enabled": true,
            "profile_background_color": "149500",
            "profile_background_image_url": "http:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_image_url_https": "https:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_tile": false,
            "profile_image_url": "http:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_image_url_https": "https:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_banner_url": "https:\/\/pbs.twimg.com\/profile_banners\/816653\/1431096993",
            "profile_link_color": "097000",
            "profile_sidebar_border_color": "FFFFFF",
            "profile_sidebar_fill_color": "DDFFCC",
            "profile_text_color": "222222",
            "profile_use_background_image": true,
            "default_profile": false,
            "default_profile_image": false,
            "following": true,
            "follow_request_sent": false,
            "notifications": false
        },
        "geo": null,
        "coordinates": null,
        "place": null,
        "contributors": null,
        "retweet_count": 20,
        "favorite_count": 5,
        "entities": {
            "hashtags": [],
            "symbols": [],
            "user_mentions": [
                {
                    "screen_name": "fredericl",
                    "name": "Frederic Lardinois",
                    "id": 2132641,
                    "id_str": "2132641",
                    "indices": [78, 88]
                }
            ],
            "urls": [
                {
                    "url": "http:\/\/t.co\/rx6byugeEs",
                    "expanded_url": "http:\/\/tcrn.ch\/1Hqbthe",
                    "display_url": "tcrn.ch\/1Hqbthe",
                    "indices": [52, 74]
                }
            ]
        },
        "favorited": false,
        "retweeted": false,
        "possibly_sensitive": false,
        "lang": "en"
    },
    {
        "created_at": "Tue May 19 16:24:30 +0000 2015",
        "id": 600698554844192768,
        "id_str": "600698554844192768",
        "text": "Microsoft Releases Preview Of Office For Android Phones http:\/\/t.co\/BAGD8lczAj by @alex",
        "source": "\u003ca href=\"http:\/\/10up.com\" rel=\"nofollow\"\u003e10up Publish Tweet\u003c\/a\u003e",
        "truncated": false,
        "in_reply_to_status_id": null,
        "in_reply_to_status_id_str": null,
        "in_reply_to_user_id": null,
        "in_reply_to_user_id_str": null,
        "in_reply_to_screen_name": null,
        "user": {
            "id": 816653,
            "id_str": "816653",
            "name": "TechCrunch",
            "screen_name": "TechCrunch",
            "location": "San Francisco, CA",
            "description": "Breaking technology news, analysis, and opinions from TechCrunch. The number one guide for all things tech.",
            "url": "http:\/\/t.co\/b5Oyx12qGG",
            "entities": {
                "url": {
                    "urls": [
                        {
                            "url": "http:\/\/t.co\/b5Oyx12qGG",
                            "expanded_url": "http:\/\/techcrunch.com",
                            "display_url": "techcrunch.com",
                            "indices": [0, 22]
                        }
                    ]
                },
                "description": {
                    "urls": []
                }
            },
            "protected": false,
            "followers_count": 5104212,
            "friends_count": 683,
            "listed_count": 97340,
            "created_at": "Wed Mar 07 01:27:09 +0000 2007",
            "favourites_count": 593,
            "utc_offset": -25200,
            "time_zone": "Pacific Time (US & Canada)",
            "geo_enabled": true,
            "verified": true,
            "statuses_count": 99688,
            "lang": "en",
            "contributors_enabled": false,
            "is_translator": false,
            "is_translation_enabled": true,
            "profile_background_color": "149500",
            "profile_background_image_url": "http:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_image_url_https": "https:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_tile": false,
            "profile_image_url": "http:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_image_url_https": "https:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_banner_url": "https:\/\/pbs.twimg.com\/profile_banners\/816653\/1431096993",
            "profile_link_color": "097000",
            "profile_sidebar_border_color": "FFFFFF",
            "profile_sidebar_fill_color": "DDFFCC",
            "profile_text_color": "222222",
            "profile_use_background_image": true,
            "default_profile": false,
            "default_profile_image": false,
            "following": true,
            "follow_request_sent": false,
            "notifications": false
        },
        "geo": null,
        "coordinates": null,
        "place": null,
        "contributors": null,
        "retweet_count": 47,
        "favorite_count": 18,
        "entities": {
            "hashtags": [],
            "symbols": [],
            "user_mentions": [
                {
                    "screen_name": "alex",
                    "name": "Alex ",
                    "id": 7380362,
                    "id_str": "7380362",
                    "indices": [82, 87]
                }
            ],
            "urls": [
                {
                    "url": "http:\/\/t.co\/BAGD8lczAj",
                    "expanded_url": "http:\/\/tcrn.ch\/1HsrPcf",
                    "display_url": "tcrn.ch\/1HsrPcf",
                    "indices": [56, 78]
                }
            ]
        },
        "favorited": false,
        "retweeted": false,
        "possibly_sensitive": false,
        "lang": "en"
    },
    {
        "created_at": "Tue May 19 16:17:12 +0000 2015",
        "id": 600696716682072064,
        "id_str": "600696716682072064",
        "text": "Nomad's Stand For Apple Watch Is Nice And Simple http:\/\/t.co\/5zqtRnTviR by @etherington",
        "source": "\u003ca href=\"http:\/\/10up.com\" rel=\"nofollow\"\u003e10up Publish Tweet\u003c\/a\u003e",
        "truncated": false,
        "in_reply_to_status_id": null,
        "in_reply_to_status_id_str": null,
        "in_reply_to_user_id": null,
        "in_reply_to_user_id_str": null,
        "in_reply_to_screen_name": null,
        "user": {
            "id": 816653,
            "id_str": "816653",
            "name": "TechCrunch",
            "screen_name": "TechCrunch",
            "location": "San Francisco, CA",
            "description": "Breaking technology news, analysis, and opinions from TechCrunch. The number one guide for all things tech.",
            "url": "http:\/\/t.co\/b5Oyx12qGG",
            "entities": {
                "url": {
                    "urls": [
                        {
                            "url": "http:\/\/t.co\/b5Oyx12qGG",
                            "expanded_url": "http:\/\/techcrunch.com",
                            "display_url": "techcrunch.com",
                            "indices": [0, 22]
                        }
                    ]
                },
                "description": {
                    "urls": []
                }
            },
            "protected": false,
            "followers_count": 5104212,
            "friends_count": 683,
            "listed_count": 97340,
            "created_at": "Wed Mar 07 01:27:09 +0000 2007",
            "favourites_count": 593,
            "utc_offset": -25200,
            "time_zone": "Pacific Time (US & Canada)",
            "geo_enabled": true,
            "verified": true,
            "statuses_count": 99688,
            "lang": "en",
            "contributors_enabled": false,
            "is_translator": false,
            "is_translation_enabled": true,
            "profile_background_color": "149500",
            "profile_background_image_url": "http:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_image_url_https": "https:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_tile": false,
            "profile_image_url": "http:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_image_url_https": "https:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_banner_url": "https:\/\/pbs.twimg.com\/profile_banners\/816653\/1431096993",
            "profile_link_color": "097000",
            "profile_sidebar_border_color": "FFFFFF",
            "profile_sidebar_fill_color": "DDFFCC",
            "profile_text_color": "222222",
            "profile_use_background_image": true,
            "default_profile": false,
            "default_profile_image": false,
            "following": true,
            "follow_request_sent": false,
            "notifications": false
        },
        "geo": null,
        "coordinates": null,
        "place": null,
        "contributors": null,
        "retweet_count": 16,
        "favorite_count": 8,
        "entities": {
            "hashtags": [],
            "symbols": [],
            "user_mentions": [
                {
                    "screen_name": "etherington",
                    "name": "Darrell Etherington",
                    "id": 15425183,
                    "id_str": "15425183",
                    "indices": [75, 87]
                }
            ],
            "urls": [
                {
                    "url": "http:\/\/t.co\/5zqtRnTviR",
                    "expanded_url": "http:\/\/tcrn.ch\/1FrOXVT",
                    "display_url": "tcrn.ch\/1FrOXVT",
                    "indices": [49, 71]
                }
            ]
        },
        "favorited": false,
        "retweeted": false,
        "possibly_sensitive": false,
        "lang": "en"
    },
    {
        "created_at": "Tue May 19 16:00:28 +0000 2015",
        "id": 600692507094069248,
        "id_str": "600692507094069248",
        "text": "OpenStack Launches Community App Catalog To Make Installing Cloud Stacks And Apps Easier http:\/\/t.co\/Q1oqYt9kNH by @fredericl",
        "source": "\u003ca href=\"http:\/\/10up.com\" rel=\"nofollow\"\u003e10up Publish Tweet\u003c\/a\u003e",
        "truncated": false,
        "in_reply_to_status_id": null,
        "in_reply_to_status_id_str": null,
        "in_reply_to_user_id": null,
        "in_reply_to_user_id_str": null,
        "in_reply_to_screen_name": null,
        "user": {
            "id": 816653,
            "id_str": "816653",
            "name": "TechCrunch",
            "screen_name": "TechCrunch",
            "location": "San Francisco, CA",
            "description": "Breaking technology news, analysis, and opinions from TechCrunch. The number one guide for all things tech.",
            "url": "http:\/\/t.co\/b5Oyx12qGG",
            "entities": {
                "url": {
                    "urls": [
                        {
                            "url": "http:\/\/t.co\/b5Oyx12qGG",
                            "expanded_url": "http:\/\/techcrunch.com",
                            "display_url": "techcrunch.com",
                            "indices": [0, 22]
                        }
                    ]
                },
                "description": {
                    "urls": []
                }
            },
            "protected": false,
            "followers_count": 5104212,
            "friends_count": 683,
            "listed_count": 97340,
            "created_at": "Wed Mar 07 01:27:09 +0000 2007",
            "favourites_count": 593,
            "utc_offset": -25200,
            "time_zone": "Pacific Time (US & Canada)",
            "geo_enabled": true,
            "verified": true,
            "statuses_count": 99688,
            "lang": "en",
            "contributors_enabled": false,
            "is_translator": false,
            "is_translation_enabled": true,
            "profile_background_color": "149500",
            "profile_background_image_url": "http:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_image_url_https": "https:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_tile": false,
            "profile_image_url": "http:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_image_url_https": "https:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_banner_url": "https:\/\/pbs.twimg.com\/profile_banners\/816653\/1431096993",
            "profile_link_color": "097000",
            "profile_sidebar_border_color": "FFFFFF",
            "profile_sidebar_fill_color": "DDFFCC",
            "profile_text_color": "222222",
            "profile_use_background_image": true,
            "default_profile": false,
            "default_profile_image": false,
            "following": true,
            "follow_request_sent": false,
            "notifications": false
        },
        "geo": null,
        "coordinates": null,
        "place": null,
        "contributors": null,
        "retweet_count": 18,
        "favorite_count": 8,
        "entities": {
            "hashtags": [],
            "symbols": [],
            "user_mentions": [
                {
                    "screen_name": "fredericl",
                    "name": "Frederic Lardinois",
                    "id": 2132641,
                    "id_str": "2132641",
                    "indices": [115, 125]
                }
            ],
            "urls": [
                {
                    "url": "http:\/\/t.co\/Q1oqYt9kNH",
                    "expanded_url": "http:\/\/tcrn.ch\/1Hsp36M",
                    "display_url": "tcrn.ch\/1Hsp36M",
                    "indices": [89, 111]
                }
            ]
        },
        "favorited": false,
        "retweeted": false,
        "possibly_sensitive": false,
        "lang": "en"
    },
    {
        "created_at": "Tue May 19 16:00:15 +0000 2015",
        "id": 600692450311544832,
        "id_str": "600692450311544832",
        "text": "Twilio Launches IP Messaging, New Conference Call Services And High-Volume SMS Tools http:\/\/t.co\/QBIYSaCn5N by @fredericl",
        "source": "\u003ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003eTwitter Web Client\u003c\/a\u003e",
        "truncated": false,
        "in_reply_to_status_id": null,
        "in_reply_to_status_id_str": null,
        "in_reply_to_user_id": null,
        "in_reply_to_user_id_str": null,
        "in_reply_to_screen_name": null,
        "user": {
            "id": 816653,
            "id_str": "816653",
            "name": "TechCrunch",
            "screen_name": "TechCrunch",
            "location": "San Francisco, CA",
            "description": "Breaking technology news, analysis, and opinions from TechCrunch. The number one guide for all things tech.",
            "url": "http:\/\/t.co\/b5Oyx12qGG",
            "entities": {
                "url": {
                    "urls": [
                        {
                            "url": "http:\/\/t.co\/b5Oyx12qGG",
                            "expanded_url": "http:\/\/techcrunch.com",
                            "display_url": "techcrunch.com",
                            "indices": [0, 22]
                        }
                    ]
                },
                "description": {
                    "urls": []
                }
            },
            "protected": false,
            "followers_count": 5104212,
            "friends_count": 683,
            "listed_count": 97340,
            "created_at": "Wed Mar 07 01:27:09 +0000 2007",
            "favourites_count": 593,
            "utc_offset": -25200,
            "time_zone": "Pacific Time (US & Canada)",
            "geo_enabled": true,
            "verified": true,
            "statuses_count": 99688,
            "lang": "en",
            "contributors_enabled": false,
            "is_translator": false,
            "is_translation_enabled": true,
            "profile_background_color": "149500",
            "profile_background_image_url": "http:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_image_url_https": "https:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_tile": false,
            "profile_image_url": "http:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_image_url_https": "https:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_banner_url": "https:\/\/pbs.twimg.com\/profile_banners\/816653\/1431096993",
            "profile_link_color": "097000",
            "profile_sidebar_border_color": "FFFFFF",
            "profile_sidebar_fill_color": "DDFFCC",
            "profile_text_color": "222222",
            "profile_use_background_image": true,
            "default_profile": false,
            "default_profile_image": false,
            "following": true,
            "follow_request_sent": false,
            "notifications": false
        },
        "geo": null,
        "coordinates": null,
        "place": null,
        "contributors": null,
        "retweet_count": 19,
        "favorite_count": 10,
        "entities": {
            "hashtags": [],
            "symbols": [],
            "user_mentions": [
                {
                    "screen_name": "fredericl",
                    "name": "Frederic Lardinois",
                    "id": 2132641,
                    "id_str": "2132641",
                    "indices": [111, 121]
                }
            ],
            "urls": [
                {
                    "url": "http:\/\/t.co\/QBIYSaCn5N",
                    "expanded_url": "http:\/\/techcrunch.com\/2015\/05\/19\/twilio-launches-ip-messaging-new-conference-call-services-and-high-volume-sms-tools\/",
                    "display_url": "techcrunch.com\/2015\/05\/19\/twi\u2026",
                    "indices": [85, 107]
                }
            ]
        },
        "favorited": false,
        "retweeted": false,
        "possibly_sensitive": false,
        "lang": "en"
    },
    {
        "created_at": "Tue May 19 15:43:38 +0000 2015",
        "id": 600688270729687040,
        "id_str": "600688270729687040",
        "text": "Little Labs Raises $3 Million From NEA And Others For Its \"Smartwatch-First\" App Studio http:\/\/t.co\/Li1EUz9ozR by @sarahintampa",
        "source": "\u003ca href=\"http:\/\/10up.com\" rel=\"nofollow\"\u003e10up Publish Tweet\u003c\/a\u003e",
        "truncated": false,
        "in_reply_to_status_id": null,
        "in_reply_to_status_id_str": null,
        "in_reply_to_user_id": null,
        "in_reply_to_user_id_str": null,
        "in_reply_to_screen_name": null,
        "user": {
            "id": 816653,
            "id_str": "816653",
            "name": "TechCrunch",
            "screen_name": "TechCrunch",
            "location": "San Francisco, CA",
            "description": "Breaking technology news, analysis, and opinions from TechCrunch. The number one guide for all things tech.",
            "url": "http:\/\/t.co\/b5Oyx12qGG",
            "entities": {
                "url": {
                    "urls": [
                        {
                            "url": "http:\/\/t.co\/b5Oyx12qGG",
                            "expanded_url": "http:\/\/techcrunch.com",
                            "display_url": "techcrunch.com",
                            "indices": [0, 22]
                        }
                    ]
                },
                "description": {
                    "urls": []
                }
            },
            "protected": false,
            "followers_count": 5104212,
            "friends_count": 683,
            "listed_count": 97340,
            "created_at": "Wed Mar 07 01:27:09 +0000 2007",
            "favourites_count": 593,
            "utc_offset": -25200,
            "time_zone": "Pacific Time (US & Canada)",
            "geo_enabled": true,
            "verified": true,
            "statuses_count": 99688,
            "lang": "en",
            "contributors_enabled": false,
            "is_translator": false,
            "is_translation_enabled": true,
            "profile_background_color": "149500",
            "profile_background_image_url": "http:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_image_url_https": "https:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_tile": false,
            "profile_image_url": "http:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_image_url_https": "https:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_banner_url": "https:\/\/pbs.twimg.com\/profile_banners\/816653\/1431096993",
            "profile_link_color": "097000",
            "profile_sidebar_border_color": "FFFFFF",
            "profile_sidebar_fill_color": "DDFFCC",
            "profile_text_color": "222222",
            "profile_use_background_image": true,
            "default_profile": false,
            "default_profile_image": false,
            "following": true,
            "follow_request_sent": false,
            "notifications": false
        },
        "geo": null,
        "coordinates": null,
        "place": null,
        "contributors": null,
        "retweet_count": 23,
        "favorite_count": 12,
        "entities": {
            "hashtags": [],
            "symbols": [],
            "user_mentions": [
                {
                    "screen_name": "sarahintampa",
                    "name": "Sarah Perez",
                    "id": 683113,
                    "id_str": "683113",
                    "indices": [114, 127]
                }
            ],
            "urls": [
                {
                    "url": "http:\/\/t.co\/Li1EUz9ozR",
                    "expanded_url": "http:\/\/tcrn.ch\/1IOt588",
                    "display_url": "tcrn.ch\/1IOt588",
                    "indices": [88, 110]
                }
            ]
        },
        "favorited": false,
        "retweeted": false,
        "possibly_sensitive": false,
        "lang": "en"
    },
    {
        "created_at": "Tue May 19 15:15:53 +0000 2015",
        "id": 600681286777380864,
        "id_str": "600681286777380864",
        "text": "MuleSoft's Massive $128M Round Could Be Precursor To IPO http:\/\/t.co\/blgPmx3OaP by @ron_miller",
        "source": "\u003ca href=\"http:\/\/10up.com\" rel=\"nofollow\"\u003e10up Publish Tweet\u003c\/a\u003e",
        "truncated": false,
        "in_reply_to_status_id": null,
        "in_reply_to_status_id_str": null,
        "in_reply_to_user_id": null,
        "in_reply_to_user_id_str": null,
        "in_reply_to_screen_name": null,
        "user": {
            "id": 816653,
            "id_str": "816653",
            "name": "TechCrunch",
            "screen_name": "TechCrunch",
            "location": "San Francisco, CA",
            "description": "Breaking technology news, analysis, and opinions from TechCrunch. The number one guide for all things tech.",
            "url": "http:\/\/t.co\/b5Oyx12qGG",
            "entities": {
                "url": {
                    "urls": [
                        {
                            "url": "http:\/\/t.co\/b5Oyx12qGG",
                            "expanded_url": "http:\/\/techcrunch.com",
                            "display_url": "techcrunch.com",
                            "indices": [0, 22]
                        }
                    ]
                },
                "description": {
                    "urls": []
                }
            },
            "protected": false,
            "followers_count": 5104212,
            "friends_count": 683,
            "listed_count": 97340,
            "created_at": "Wed Mar 07 01:27:09 +0000 2007",
            "favourites_count": 593,
            "utc_offset": -25200,
            "time_zone": "Pacific Time (US & Canada)",
            "geo_enabled": true,
            "verified": true,
            "statuses_count": 99688,
            "lang": "en",
            "contributors_enabled": false,
            "is_translator": false,
            "is_translation_enabled": true,
            "profile_background_color": "149500",
            "profile_background_image_url": "http:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_image_url_https": "https:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_tile": false,
            "profile_image_url": "http:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_image_url_https": "https:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_banner_url": "https:\/\/pbs.twimg.com\/profile_banners\/816653\/1431096993",
            "profile_link_color": "097000",
            "profile_sidebar_border_color": "FFFFFF",
            "profile_sidebar_fill_color": "DDFFCC",
            "profile_text_color": "222222",
            "profile_use_background_image": true,
            "default_profile": false,
            "default_profile_image": false,
            "following": true,
            "follow_request_sent": false,
            "notifications": false
        },
        "geo": null,
        "coordinates": null,
        "place": null,
        "contributors": null,
        "retweet_count": 26,
        "favorite_count": 8,
        "entities": {
            "hashtags": [],
            "symbols": [],
            "user_mentions": [
                {
                    "screen_name": "ron_miller",
                    "name": "Ron Miller",
                    "id": 5874452,
                    "id_str": "5874452",
                    "indices": [83, 94]
                }
            ],
            "urls": [
                {
                    "url": "http:\/\/t.co\/blgPmx3OaP",
                    "expanded_url": "http:\/\/tcrn.ch\/1IOnH5b",
                    "display_url": "tcrn.ch\/1IOnH5b",
                    "indices": [57, 79]
                }
            ]
        },
        "favorited": false,
        "retweeted": false,
        "possibly_sensitive": false,
        "lang": "en"
    },
    {
        "created_at": "Tue May 19 14:39:49 +0000 2015",
        "id": 600672208399142913,
        "id_str": "600672208399142913",
        "text": "iSpot\u2024tv Raises $21.9M To Track The Online Impact Of TV Advertising http:\/\/t.co\/fiE8tNz78c by @anthonyha",
        "source": "\u003ca href=\"http:\/\/10up.com\" rel=\"nofollow\"\u003e10up Publish Tweet\u003c\/a\u003e",
        "truncated": false,
        "in_reply_to_status_id": null,
        "in_reply_to_status_id_str": null,
        "in_reply_to_user_id": null,
        "in_reply_to_user_id_str": null,
        "in_reply_to_screen_name": null,
        "user": {
            "id": 816653,
            "id_str": "816653",
            "name": "TechCrunch",
            "screen_name": "TechCrunch",
            "location": "San Francisco, CA",
            "description": "Breaking technology news, analysis, and opinions from TechCrunch. The number one guide for all things tech.",
            "url": "http:\/\/t.co\/b5Oyx12qGG",
            "entities": {
                "url": {
                    "urls": [
                        {
                            "url": "http:\/\/t.co\/b5Oyx12qGG",
                            "expanded_url": "http:\/\/techcrunch.com",
                            "display_url": "techcrunch.com",
                            "indices": [0, 22]
                        }
                    ]
                },
                "description": {
                    "urls": []
                }
            },
            "protected": false,
            "followers_count": 5104212,
            "friends_count": 683,
            "listed_count": 97340,
            "created_at": "Wed Mar 07 01:27:09 +0000 2007",
            "favourites_count": 593,
            "utc_offset": -25200,
            "time_zone": "Pacific Time (US & Canada)",
            "geo_enabled": true,
            "verified": true,
            "statuses_count": 99688,
            "lang": "en",
            "contributors_enabled": false,
            "is_translator": false,
            "is_translation_enabled": true,
            "profile_background_color": "149500",
            "profile_background_image_url": "http:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_image_url_https": "https:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_tile": false,
            "profile_image_url": "http:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_image_url_https": "https:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_banner_url": "https:\/\/pbs.twimg.com\/profile_banners\/816653\/1431096993",
            "profile_link_color": "097000",
            "profile_sidebar_border_color": "FFFFFF",
            "profile_sidebar_fill_color": "DDFFCC",
            "profile_text_color": "222222",
            "profile_use_background_image": true,
            "default_profile": false,
            "default_profile_image": false,
            "following": true,
            "follow_request_sent": false,
            "notifications": false
        },
        "geo": null,
        "coordinates": null,
        "place": null,
        "contributors": null,
        "retweet_count": 24,
        "favorite_count": 11,
        "entities": {
            "hashtags": [],
            "symbols": [],
            "user_mentions": [
                {
                    "screen_name": "anthonyha",
                    "name": "Anthony Ha",
                    "id": 14269152,
                    "id_str": "14269152",
                    "indices": [94, 104]
                }
            ],
            "urls": [
                {
                    "url": "http:\/\/t.co\/fiE8tNz78c",
                    "expanded_url": "http:\/\/tcrn.ch\/1EZLLgJ",
                    "display_url": "tcrn.ch\/1EZLLgJ",
                    "indices": [68, 90]
                }
            ]
        },
        "favorited": false,
        "retweeted": false,
        "possibly_sensitive": false,
        "lang": "en"
    },
    {
        "created_at": "Tue May 19 14:00:46 +0000 2015",
        "id": 600662383699238913,
        "id_str": "600662383699238913",
        "text": "Pinterest Unveils Its First Video-Like Ad http:\/\/t.co\/HOy8qO6mCz by @mattlynley",
        "source": "\u003ca href=\"http:\/\/10up.com\" rel=\"nofollow\"\u003e10up Publish Tweet\u003c\/a\u003e",
        "truncated": false,
        "in_reply_to_status_id": null,
        "in_reply_to_status_id_str": null,
        "in_reply_to_user_id": null,
        "in_reply_to_user_id_str": null,
        "in_reply_to_screen_name": null,
        "user": {
            "id": 816653,
            "id_str": "816653",
            "name": "TechCrunch",
            "screen_name": "TechCrunch",
            "location": "San Francisco, CA",
            "description": "Breaking technology news, analysis, and opinions from TechCrunch. The number one guide for all things tech.",
            "url": "http:\/\/t.co\/b5Oyx12qGG",
            "entities": {
                "url": {
                    "urls": [
                        {
                            "url": "http:\/\/t.co\/b5Oyx12qGG",
                            "expanded_url": "http:\/\/techcrunch.com",
                            "display_url": "techcrunch.com",
                            "indices": [0, 22]
                        }
                    ]
                },
                "description": {
                    "urls": []
                }
            },
            "protected": false,
            "followers_count": 5104212,
            "friends_count": 683,
            "listed_count": 97340,
            "created_at": "Wed Mar 07 01:27:09 +0000 2007",
            "favourites_count": 593,
            "utc_offset": -25200,
            "time_zone": "Pacific Time (US & Canada)",
            "geo_enabled": true,
            "verified": true,
            "statuses_count": 99688,
            "lang": "en",
            "contributors_enabled": false,
            "is_translator": false,
            "is_translation_enabled": true,
            "profile_background_color": "149500",
            "profile_background_image_url": "http:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_image_url_https": "https:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_tile": false,
            "profile_image_url": "http:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_image_url_https": "https:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_banner_url": "https:\/\/pbs.twimg.com\/profile_banners\/816653\/1431096993",
            "profile_link_color": "097000",
            "profile_sidebar_border_color": "FFFFFF",
            "profile_sidebar_fill_color": "DDFFCC",
            "profile_text_color": "222222",
            "profile_use_background_image": true,
            "default_profile": false,
            "default_profile_image": false,
            "following": true,
            "follow_request_sent": false,
            "notifications": false
        },
        "geo": null,
        "coordinates": null,
        "place": null,
        "contributors": null,
        "retweet_count": 35,
        "favorite_count": 13,
        "entities": {
            "hashtags": [],
            "symbols": [],
            "user_mentions": [
                {
                    "screen_name": "mattlynley",
                    "name": "Matthew Lynley",
                    "id": 13146722,
                    "id_str": "13146722",
                    "indices": [68, 79]
                }
            ],
            "urls": [
                {
                    "url": "http:\/\/t.co\/HOy8qO6mCz",
                    "expanded_url": "http:\/\/tcrn.ch\/1EV0loH",
                    "display_url": "tcrn.ch\/1EV0loH",
                    "indices": [42, 64]
                }
            ]
        },
        "favorited": false,
        "retweeted": false,
        "possibly_sensitive": false,
        "lang": "en"
    },
    {
        "created_at": "Tue May 19 14:00:32 +0000 2015",
        "id": 600662324723261441,
        "id_str": "600662324723261441",
        "text": "Meetup Targets Businesses And Corporations With Launch Of Meetup Pro http:\/\/t.co\/dhLBAD8h0a by @sarahintampa",
        "source": "\u003ca href=\"http:\/\/10up.com\" rel=\"nofollow\"\u003e10up Publish Tweet\u003c\/a\u003e",
        "truncated": false,
        "in_reply_to_status_id": null,
        "in_reply_to_status_id_str": null,
        "in_reply_to_user_id": null,
        "in_reply_to_user_id_str": null,
        "in_reply_to_screen_name": null,
        "user": {
            "id": 816653,
            "id_str": "816653",
            "name": "TechCrunch",
            "screen_name": "TechCrunch",
            "location": "San Francisco, CA",
            "description": "Breaking technology news, analysis, and opinions from TechCrunch. The number one guide for all things tech.",
            "url": "http:\/\/t.co\/b5Oyx12qGG",
            "entities": {
                "url": {
                    "urls": [
                        {
                            "url": "http:\/\/t.co\/b5Oyx12qGG",
                            "expanded_url": "http:\/\/techcrunch.com",
                            "display_url": "techcrunch.com",
                            "indices": [0, 22]
                        }
                    ]
                },
                "description": {
                    "urls": []
                }
            },
            "protected": false,
            "followers_count": 5104212,
            "friends_count": 683,
            "listed_count": 97340,
            "created_at": "Wed Mar 07 01:27:09 +0000 2007",
            "favourites_count": 593,
            "utc_offset": -25200,
            "time_zone": "Pacific Time (US & Canada)",
            "geo_enabled": true,
            "verified": true,
            "statuses_count": 99688,
            "lang": "en",
            "contributors_enabled": false,
            "is_translator": false,
            "is_translation_enabled": true,
            "profile_background_color": "149500",
            "profile_background_image_url": "http:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_image_url_https": "https:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_tile": false,
            "profile_image_url": "http:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_image_url_https": "https:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_banner_url": "https:\/\/pbs.twimg.com\/profile_banners\/816653\/1431096993",
            "profile_link_color": "097000",
            "profile_sidebar_border_color": "FFFFFF",
            "profile_sidebar_fill_color": "DDFFCC",
            "profile_text_color": "222222",
            "profile_use_background_image": true,
            "default_profile": false,
            "default_profile_image": false,
            "following": true,
            "follow_request_sent": false,
            "notifications": false
        },
        "geo": null,
        "coordinates": null,
        "place": null,
        "contributors": null,
        "retweet_count": 29,
        "favorite_count": 17,
        "entities": {
            "hashtags": [],
            "symbols": [],
            "user_mentions": [
                {
                    "screen_name": "sarahintampa",
                    "name": "Sarah Perez",
                    "id": 683113,
                    "id_str": "683113",
                    "indices": [95, 108]
                }
            ],
            "urls": [
                {
                    "url": "http:\/\/t.co\/dhLBAD8h0a",
                    "expanded_url": "http:\/\/tcrn.ch\/1edeisA",
                    "display_url": "tcrn.ch\/1edeisA",
                    "indices": [69, 91]
                }
            ]
        },
        "favorited": false,
        "retweeted": false,
        "possibly_sensitive": false,
        "lang": "en"
    },
    {
        "created_at": "Tue May 19 14:00:25 +0000 2015",
        "id": 600662295329411072,
        "id_str": "600662295329411072",
        "text": "Basis Launches A Limited Edition Titanium Peak, Updates Bands And App http:\/\/t.co\/BCVBXPwTy3 by @kylebrussell",
        "source": "\u003ca href=\"http:\/\/10up.com\" rel=\"nofollow\"\u003e10up Publish Tweet\u003c\/a\u003e",
        "truncated": false,
        "in_reply_to_status_id": null,
        "in_reply_to_status_id_str": null,
        "in_reply_to_user_id": null,
        "in_reply_to_user_id_str": null,
        "in_reply_to_screen_name": null,
        "user": {
            "id": 816653,
            "id_str": "816653",
            "name": "TechCrunch",
            "screen_name": "TechCrunch",
            "location": "San Francisco, CA",
            "description": "Breaking technology news, analysis, and opinions from TechCrunch. The number one guide for all things tech.",
            "url": "http:\/\/t.co\/b5Oyx12qGG",
            "entities": {
                "url": {
                    "urls": [
                        {
                            "url": "http:\/\/t.co\/b5Oyx12qGG",
                            "expanded_url": "http:\/\/techcrunch.com",
                            "display_url": "techcrunch.com",
                            "indices": [0, 22]
                        }
                    ]
                },
                "description": {
                    "urls": []
                }
            },
            "protected": false,
            "followers_count": 5104212,
            "friends_count": 683,
            "listed_count": 97340,
            "created_at": "Wed Mar 07 01:27:09 +0000 2007",
            "favourites_count": 593,
            "utc_offset": -25200,
            "time_zone": "Pacific Time (US & Canada)",
            "geo_enabled": true,
            "verified": true,
            "statuses_count": 99688,
            "lang": "en",
            "contributors_enabled": false,
            "is_translator": false,
            "is_translation_enabled": true,
            "profile_background_color": "149500",
            "profile_background_image_url": "http:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_image_url_https": "https:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_tile": false,
            "profile_image_url": "http:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_image_url_https": "https:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_banner_url": "https:\/\/pbs.twimg.com\/profile_banners\/816653\/1431096993",
            "profile_link_color": "097000",
            "profile_sidebar_border_color": "FFFFFF",
            "profile_sidebar_fill_color": "DDFFCC",
            "profile_text_color": "222222",
            "profile_use_background_image": true,
            "default_profile": false,
            "default_profile_image": false,
            "following": true,
            "follow_request_sent": false,
            "notifications": false
        },
        "geo": null,
        "coordinates": null,
        "place": null,
        "contributors": null,
        "retweet_count": 10,
        "favorite_count": 6,
        "entities": {
            "hashtags": [],
            "symbols": [],
            "user_mentions": [
                {
                    "screen_name": "kylebrussell",
                    "name": "Kyle Russell",
                    "id": 52247685,
                    "id_str": "52247685",
                    "indices": [96, 109]
                }
            ],
            "urls": [
                {
                    "url": "http:\/\/t.co\/BCVBXPwTy3",
                    "expanded_url": "http:\/\/tcrn.ch\/1HeyYr9",
                    "display_url": "tcrn.ch\/1HeyYr9",
                    "indices": [70, 92]
                }
            ]
        },
        "favorited": false,
        "retweeted": false,
        "possibly_sensitive": false,
        "lang": "en"
    },
    {
        "created_at": "Tue May 19 13:53:56 +0000 2015",
        "id": 600660661169889282,
        "id_str": "600660661169889282",
        "text": "Apple Now Sells A Lightning Dock For Your iPhone http:\/\/t.co\/CIFSFayNPp by @etherington http:\/\/t.co\/OHuBQ0SwXX",
        "source": "\u003ca href=\"http:\/\/10up.com\" rel=\"nofollow\"\u003e10up Publish Tweet\u003c\/a\u003e",
        "truncated": false,
        "in_reply_to_status_id": null,
        "in_reply_to_status_id_str": null,
        "in_reply_to_user_id": null,
        "in_reply_to_user_id_str": null,
        "in_reply_to_screen_name": null,
        "user": {
            "id": 816653,
            "id_str": "816653",
            "name": "TechCrunch",
            "screen_name": "TechCrunch",
            "location": "San Francisco, CA",
            "description": "Breaking technology news, analysis, and opinions from TechCrunch. The number one guide for all things tech.",
            "url": "http:\/\/t.co\/b5Oyx12qGG",
            "entities": {
                "url": {
                    "urls": [
                        {
                            "url": "http:\/\/t.co\/b5Oyx12qGG",
                            "expanded_url": "http:\/\/techcrunch.com",
                            "display_url": "techcrunch.com",
                            "indices": [0, 22]
                        }
                    ]
                },
                "description": {
                    "urls": []
                }
            },
            "protected": false,
            "followers_count": 5104212,
            "friends_count": 683,
            "listed_count": 97340,
            "created_at": "Wed Mar 07 01:27:09 +0000 2007",
            "favourites_count": 593,
            "utc_offset": -25200,
            "time_zone": "Pacific Time (US & Canada)",
            "geo_enabled": true,
            "verified": true,
            "statuses_count": 99688,
            "lang": "en",
            "contributors_enabled": false,
            "is_translator": false,
            "is_translation_enabled": true,
            "profile_background_color": "149500",
            "profile_background_image_url": "http:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_image_url_https": "https:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_tile": false,
            "profile_image_url": "http:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_image_url_https": "https:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_banner_url": "https:\/\/pbs.twimg.com\/profile_banners\/816653\/1431096993",
            "profile_link_color": "097000",
            "profile_sidebar_border_color": "FFFFFF",
            "profile_sidebar_fill_color": "DDFFCC",
            "profile_text_color": "222222",
            "profile_use_background_image": true,
            "default_profile": false,
            "default_profile_image": false,
            "following": true,
            "follow_request_sent": false,
            "notifications": false
        },
        "geo": null,
        "coordinates": null,
        "place": null,
        "contributors": null,
        "retweet_count": 54,
        "favorite_count": 43,
        "entities": {
            "hashtags": [],
            "symbols": [],
            "user_mentions": [
                {
                    "screen_name": "etherington",
                    "name": "Darrell Etherington",
                    "id": 15425183,
                    "id_str": "15425183",
                    "indices": [75, 87]
                }
            ],
            "urls": [
                {
                    "url": "http:\/\/t.co\/CIFSFayNPp",
                    "expanded_url": "http:\/\/tcrn.ch\/1eddOCH",
                    "display_url": "tcrn.ch\/1eddOCH",
                    "indices": [49, 71]
                }
            ],
            "media": [
                {
                    "id": 600660660171608064,
                    "id_str": "600660660171608064",
                    "indices": [88, 110],
                    "media_url": "http:\/\/pbs.twimg.com\/media\/CFX5sCAUkAA1vKk.jpg",
                    "media_url_https": "https:\/\/pbs.twimg.com\/media\/CFX5sCAUkAA1vKk.jpg",
                    "url": "http:\/\/t.co\/OHuBQ0SwXX",
                    "display_url": "pic.twitter.com\/OHuBQ0SwXX",
                    "expanded_url": "http:\/\/twitter.com\/TechCrunch\/status\/600660661169889282\/photo\/1",
                    "type": "photo",
                    "sizes": {
                        "thumb": {
                            "w": 150,
                            "h": 150,
                            "resize": "crop"
                        },
                        "small": {
                            "w": 340,
                            "h": 340,
                            "resize": "fit"
                        },
                        "medium": {
                            "w": 600,
                            "h": 600,
                            "resize": "fit"
                        },
                        "large": {
                            "w": 1024,
                            "h": 1024,
                            "resize": "fit"
                        }
                    }
                }
            ]
        },
        "extended_entities": {
            "media": [
                {
                    "id": 600660660171608064,
                    "id_str": "600660660171608064",
                    "indices": [88, 110],
                    "media_url": "http:\/\/pbs.twimg.com\/media\/CFX5sCAUkAA1vKk.jpg",
                    "media_url_https": "https:\/\/pbs.twimg.com\/media\/CFX5sCAUkAA1vKk.jpg",
                    "url": "http:\/\/t.co\/OHuBQ0SwXX",
                    "display_url": "pic.twitter.com\/OHuBQ0SwXX",
                    "expanded_url": "http:\/\/twitter.com\/TechCrunch\/status\/600660661169889282\/photo\/1",
                    "type": "photo",
                    "sizes": {
                        "thumb": {
                            "w": 150,
                            "h": 150,
                            "resize": "crop"
                        },
                        "small": {
                            "w": 340,
                            "h": 340,
                            "resize": "fit"
                        },
                        "medium": {
                            "w": 600,
                            "h": 600,
                            "resize": "fit"
                        },
                        "large": {
                            "w": 1024,
                            "h": 1024,
                            "resize": "fit"
                        }
                    }
                }
            ]
        },
        "favorited": false,
        "retweeted": false,
        "possibly_sensitive": false,
        "lang": "en"
    },
    {
        "created_at": "Tue May 19 13:37:02 +0000 2015",
        "id": 600656409978474497,
        "id_str": "600656409978474497",
        "text": "Oru Seeks Adventure With The Coast Folding Expedition Kayak http:\/\/t.co\/3k7Rbcfh8Y by @etherington http:\/\/t.co\/9C9mXXHlyS",
        "source": "\u003ca href=\"http:\/\/10up.com\" rel=\"nofollow\"\u003e10up Publish Tweet\u003c\/a\u003e",
        "truncated": false,
        "in_reply_to_status_id": null,
        "in_reply_to_status_id_str": null,
        "in_reply_to_user_id": null,
        "in_reply_to_user_id_str": null,
        "in_reply_to_screen_name": null,
        "user": {
            "id": 816653,
            "id_str": "816653",
            "name": "TechCrunch",
            "screen_name": "TechCrunch",
            "location": "San Francisco, CA",
            "description": "Breaking technology news, analysis, and opinions from TechCrunch. The number one guide for all things tech.",
            "url": "http:\/\/t.co\/b5Oyx12qGG",
            "entities": {
                "url": {
                    "urls": [
                        {
                            "url": "http:\/\/t.co\/b5Oyx12qGG",
                            "expanded_url": "http:\/\/techcrunch.com",
                            "display_url": "techcrunch.com",
                            "indices": [0, 22]
                        }
                    ]
                },
                "description": {
                    "urls": []
                }
            },
            "protected": false,
            "followers_count": 5104212,
            "friends_count": 683,
            "listed_count": 97340,
            "created_at": "Wed Mar 07 01:27:09 +0000 2007",
            "favourites_count": 593,
            "utc_offset": -25200,
            "time_zone": "Pacific Time (US & Canada)",
            "geo_enabled": true,
            "verified": true,
            "statuses_count": 99688,
            "lang": "en",
            "contributors_enabled": false,
            "is_translator": false,
            "is_translation_enabled": true,
            "profile_background_color": "149500",
            "profile_background_image_url": "http:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_image_url_https": "https:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_tile": false,
            "profile_image_url": "http:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_image_url_https": "https:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_banner_url": "https:\/\/pbs.twimg.com\/profile_banners\/816653\/1431096993",
            "profile_link_color": "097000",
            "profile_sidebar_border_color": "FFFFFF",
            "profile_sidebar_fill_color": "DDFFCC",
            "profile_text_color": "222222",
            "profile_use_background_image": true,
            "default_profile": false,
            "default_profile_image": false,
            "following": true,
            "follow_request_sent": false,
            "notifications": false
        },
        "geo": null,
        "coordinates": null,
        "place": null,
        "contributors": null,
        "retweet_count": 27,
        "favorite_count": 24,
        "entities": {
            "hashtags": [],
            "symbols": [],
            "user_mentions": [
                {
                    "screen_name": "etherington",
                    "name": "Darrell Etherington",
                    "id": 15425183,
                    "id_str": "15425183",
                    "indices": [86, 98]
                }
            ],
            "urls": [
                {
                    "url": "http:\/\/t.co\/3k7Rbcfh8Y",
                    "expanded_url": "http:\/\/tcrn.ch\/1IOc6CV",
                    "display_url": "tcrn.ch\/1IOc6CV",
                    "indices": [60, 82]
                }
            ],
            "media": [
                {
                    "id": 600656408892149763,
                    "id_str": "600656408892149763",
                    "indices": [99, 121],
                    "media_url": "http:\/\/pbs.twimg.com\/tweet_video_thumb\/CFX10kwUEAMCVQE.png",
                    "media_url_https": "https:\/\/pbs.twimg.com\/tweet_video_thumb\/CFX10kwUEAMCVQE.png",
                    "url": "http:\/\/t.co\/9C9mXXHlyS",
                    "display_url": "pic.twitter.com\/9C9mXXHlyS",
                    "expanded_url": "http:\/\/twitter.com\/TechCrunch\/status\/600656409978474497\/photo\/1",
                    "type": "photo",
                    "sizes": {
                        "large": {
                            "w": 700,
                            "h": 252,
                            "resize": "fit"
                        },
                        "small": {
                            "w": 340,
                            "h": 122,
                            "resize": "fit"
                        },
                        "thumb": {
                            "w": 150,
                            "h": 150,
                            "resize": "crop"
                        },
                        "medium": {
                            "w": 600,
                            "h": 216,
                            "resize": "fit"
                        }
                    }
                }
            ]
        },
        "extended_entities": {
            "media": [
                {
                    "id": 600656408892149763,
                    "id_str": "600656408892149763",
                    "indices": [99, 121],
                    "media_url": "http:\/\/pbs.twimg.com\/tweet_video_thumb\/CFX10kwUEAMCVQE.png",
                    "media_url_https": "https:\/\/pbs.twimg.com\/tweet_video_thumb\/CFX10kwUEAMCVQE.png",
                    "url": "http:\/\/t.co\/9C9mXXHlyS",
                    "display_url": "pic.twitter.com\/9C9mXXHlyS",
                    "expanded_url": "http:\/\/twitter.com\/TechCrunch\/status\/600656409978474497\/photo\/1",
                    "type": "animated_gif",
                    "sizes": {
                        "large": {
                            "w": 700,
                            "h": 252,
                            "resize": "fit"
                        },
                        "small": {
                            "w": 340,
                            "h": 122,
                            "resize": "fit"
                        },
                        "thumb": {
                            "w": 150,
                            "h": 150,
                            "resize": "crop"
                        },
                        "medium": {
                            "w": 600,
                            "h": 216,
                            "resize": "fit"
                        }
                    },
                    "video_info": {
                        "aspect_ratio": [25, 9],
                        "variants": [
                            {
                                "bitrate": 0,
                                "content_type": "video\/mp4",
                                "url": "https:\/\/pbs.twimg.com\/tweet_video\/CFX10kwUEAMCVQE.mp4"
                            }
                        ]
                    }
                }
            ]
        },
        "favorited": false,
        "retweeted": false,
        "possibly_sensitive": false,
        "lang": "en"
    },
    {
        "created_at": "Tue May 19 13:31:28 +0000 2015",
        "id": 600655010305863680,
        "id_str": "600655010305863680",
        "text": "Vivint Launches A New Home Automation System Complete With A Tiny Doorbell Camera http:\/\/t.co\/c5UgKTxFCa by @mjburnsy",
        "source": "\u003ca href=\"http:\/\/10up.com\" rel=\"nofollow\"\u003e10up Publish Tweet\u003c\/a\u003e",
        "truncated": false,
        "in_reply_to_status_id": null,
        "in_reply_to_status_id_str": null,
        "in_reply_to_user_id": null,
        "in_reply_to_user_id_str": null,
        "in_reply_to_screen_name": null,
        "user": {
            "id": 816653,
            "id_str": "816653",
            "name": "TechCrunch",
            "screen_name": "TechCrunch",
            "location": "San Francisco, CA",
            "description": "Breaking technology news, analysis, and opinions from TechCrunch. The number one guide for all things tech.",
            "url": "http:\/\/t.co\/b5Oyx12qGG",
            "entities": {
                "url": {
                    "urls": [
                        {
                            "url": "http:\/\/t.co\/b5Oyx12qGG",
                            "expanded_url": "http:\/\/techcrunch.com",
                            "display_url": "techcrunch.com",
                            "indices": [0, 22]
                        }
                    ]
                },
                "description": {
                    "urls": []
                }
            },
            "protected": false,
            "followers_count": 5104212,
            "friends_count": 683,
            "listed_count": 97340,
            "created_at": "Wed Mar 07 01:27:09 +0000 2007",
            "favourites_count": 593,
            "utc_offset": -25200,
            "time_zone": "Pacific Time (US & Canada)",
            "geo_enabled": true,
            "verified": true,
            "statuses_count": 99688,
            "lang": "en",
            "contributors_enabled": false,
            "is_translator": false,
            "is_translation_enabled": true,
            "profile_background_color": "149500",
            "profile_background_image_url": "http:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_image_url_https": "https:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_tile": false,
            "profile_image_url": "http:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_image_url_https": "https:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_banner_url": "https:\/\/pbs.twimg.com\/profile_banners\/816653\/1431096993",
            "profile_link_color": "097000",
            "profile_sidebar_border_color": "FFFFFF",
            "profile_sidebar_fill_color": "DDFFCC",
            "profile_text_color": "222222",
            "profile_use_background_image": true,
            "default_profile": false,
            "default_profile_image": false,
            "following": true,
            "follow_request_sent": false,
            "notifications": false
        },
        "geo": null,
        "coordinates": null,
        "place": null,
        "contributors": null,
        "retweet_count": 32,
        "favorite_count": 32,
        "entities": {
            "hashtags": [],
            "symbols": [],
            "user_mentions": [
                {
                    "screen_name": "mjburnsy",
                    "name": "Matt Burns",
                    "id": 17223098,
                    "id_str": "17223098",
                    "indices": [108, 117]
                }
            ],
            "urls": [
                {
                    "url": "http:\/\/t.co\/c5UgKTxFCa",
                    "expanded_url": "http:\/\/tcrn.ch\/1HpFzBD",
                    "display_url": "tcrn.ch\/1HpFzBD",
                    "indices": [82, 104]
                }
            ]
        },
        "favorited": false,
        "retweeted": false,
        "possibly_sensitive": false,
        "lang": "en"
    },
    {
        "created_at": "Tue May 19 13:00:48 +0000 2015",
        "id": 600647293809725440,
        "id_str": "600647293809725440",
        "text": "Yota Launches Crowdfunder To Get Its Duel Screen E-Ink Smartphone To The U\u2024S. http:\/\/t.co\/bhPGIwZwgO by @riptari",
        "source": "\u003ca href=\"http:\/\/10up.com\" rel=\"nofollow\"\u003e10up Publish Tweet\u003c\/a\u003e",
        "truncated": false,
        "in_reply_to_status_id": null,
        "in_reply_to_status_id_str": null,
        "in_reply_to_user_id": null,
        "in_reply_to_user_id_str": null,
        "in_reply_to_screen_name": null,
        "user": {
            "id": 816653,
            "id_str": "816653",
            "name": "TechCrunch",
            "screen_name": "TechCrunch",
            "location": "San Francisco, CA",
            "description": "Breaking technology news, analysis, and opinions from TechCrunch. The number one guide for all things tech.",
            "url": "http:\/\/t.co\/b5Oyx12qGG",
            "entities": {
                "url": {
                    "urls": [
                        {
                            "url": "http:\/\/t.co\/b5Oyx12qGG",
                            "expanded_url": "http:\/\/techcrunch.com",
                            "display_url": "techcrunch.com",
                            "indices": [0, 22]
                        }
                    ]
                },
                "description": {
                    "urls": []
                }
            },
            "protected": false,
            "followers_count": 5104212,
            "friends_count": 683,
            "listed_count": 97340,
            "created_at": "Wed Mar 07 01:27:09 +0000 2007",
            "favourites_count": 593,
            "utc_offset": -25200,
            "time_zone": "Pacific Time (US & Canada)",
            "geo_enabled": true,
            "verified": true,
            "statuses_count": 99688,
            "lang": "en",
            "contributors_enabled": false,
            "is_translator": false,
            "is_translation_enabled": true,
            "profile_background_color": "149500",
            "profile_background_image_url": "http:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_image_url_https": "https:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_tile": false,
            "profile_image_url": "http:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_image_url_https": "https:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_banner_url": "https:\/\/pbs.twimg.com\/profile_banners\/816653\/1431096993",
            "profile_link_color": "097000",
            "profile_sidebar_border_color": "FFFFFF",
            "profile_sidebar_fill_color": "DDFFCC",
            "profile_text_color": "222222",
            "profile_use_background_image": true,
            "default_profile": false,
            "default_profile_image": false,
            "following": true,
            "follow_request_sent": false,
            "notifications": false
        },
        "geo": null,
        "coordinates": null,
        "place": null,
        "contributors": null,
        "retweet_count": 9,
        "favorite_count": 10,
        "entities": {
            "hashtags": [],
            "symbols": [],
            "user_mentions": [
                {
                    "screen_name": "riptari",
                    "name": "Natasha",
                    "id": 15734027,
                    "id_str": "15734027",
                    "indices": [104, 112]
                }
            ],
            "urls": [
                {
                    "url": "http:\/\/t.co\/bhPGIwZwgO",
                    "expanded_url": "http:\/\/tcrn.ch\/1KhSidd",
                    "display_url": "tcrn.ch\/1KhSidd",
                    "indices": [78, 100]
                }
            ]
        },
        "favorited": false,
        "retweeted": false,
        "possibly_sensitive": false,
        "lang": "en"
    },
    {
        "created_at": "Tue May 19 13:00:26 +0000 2015",
        "id": 600647200788480001,
        "id_str": "600647200788480001",
        "text": "Handy Expands Its Vacation Rental Cleaning Service http:\/\/t.co\/hRyK2rVy6c by @riptari",
        "source": "\u003ca href=\"http:\/\/10up.com\" rel=\"nofollow\"\u003e10up Publish Tweet\u003c\/a\u003e",
        "truncated": false,
        "in_reply_to_status_id": null,
        "in_reply_to_status_id_str": null,
        "in_reply_to_user_id": null,
        "in_reply_to_user_id_str": null,
        "in_reply_to_screen_name": null,
        "user": {
            "id": 816653,
            "id_str": "816653",
            "name": "TechCrunch",
            "screen_name": "TechCrunch",
            "location": "San Francisco, CA",
            "description": "Breaking technology news, analysis, and opinions from TechCrunch. The number one guide for all things tech.",
            "url": "http:\/\/t.co\/b5Oyx12qGG",
            "entities": {
                "url": {
                    "urls": [
                        {
                            "url": "http:\/\/t.co\/b5Oyx12qGG",
                            "expanded_url": "http:\/\/techcrunch.com",
                            "display_url": "techcrunch.com",
                            "indices": [0, 22]
                        }
                    ]
                },
                "description": {
                    "urls": []
                }
            },
            "protected": false,
            "followers_count": 5104212,
            "friends_count": 683,
            "listed_count": 97340,
            "created_at": "Wed Mar 07 01:27:09 +0000 2007",
            "favourites_count": 593,
            "utc_offset": -25200,
            "time_zone": "Pacific Time (US & Canada)",
            "geo_enabled": true,
            "verified": true,
            "statuses_count": 99688,
            "lang": "en",
            "contributors_enabled": false,
            "is_translator": false,
            "is_translation_enabled": true,
            "profile_background_color": "149500",
            "profile_background_image_url": "http:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_image_url_https": "https:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_tile": false,
            "profile_image_url": "http:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_image_url_https": "https:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_banner_url": "https:\/\/pbs.twimg.com\/profile_banners\/816653\/1431096993",
            "profile_link_color": "097000",
            "profile_sidebar_border_color": "FFFFFF",
            "profile_sidebar_fill_color": "DDFFCC",
            "profile_text_color": "222222",
            "profile_use_background_image": true,
            "default_profile": false,
            "default_profile_image": false,
            "following": true,
            "follow_request_sent": false,
            "notifications": false
        },
        "geo": null,
        "coordinates": null,
        "place": null,
        "contributors": null,
        "retweet_count": 15,
        "favorite_count": 13,
        "entities": {
            "hashtags": [],
            "symbols": [],
            "user_mentions": [
                {
                    "screen_name": "riptari",
                    "name": "Natasha",
                    "id": 15734027,
                    "id_str": "15734027",
                    "indices": [77, 85]
                }
            ],
            "urls": [
                {
                    "url": "http:\/\/t.co\/hRyK2rVy6c",
                    "expanded_url": "http:\/\/tcrn.ch\/1HpBAow",
                    "display_url": "tcrn.ch\/1HpBAow",
                    "indices": [51, 73]
                }
            ]
        },
        "favorited": false,
        "retweeted": false,
        "possibly_sensitive": false,
        "lang": "en"
    },
    {
        "created_at": "Tue May 19 13:00:20 +0000 2015",
        "id": 600647175761240065,
        "id_str": "600647175761240065",
        "text": "As OpenStack Matures, IBM Wants A Piece Of The Action http:\/\/t.co\/zOZoYwQJak by @ron_miller",
        "source": "\u003ca href=\"http:\/\/10up.com\" rel=\"nofollow\"\u003e10up Publish Tweet\u003c\/a\u003e",
        "truncated": false,
        "in_reply_to_status_id": null,
        "in_reply_to_status_id_str": null,
        "in_reply_to_user_id": null,
        "in_reply_to_user_id_str": null,
        "in_reply_to_screen_name": null,
        "user": {
            "id": 816653,
            "id_str": "816653",
            "name": "TechCrunch",
            "screen_name": "TechCrunch",
            "location": "San Francisco, CA",
            "description": "Breaking technology news, analysis, and opinions from TechCrunch. The number one guide for all things tech.",
            "url": "http:\/\/t.co\/b5Oyx12qGG",
            "entities": {
                "url": {
                    "urls": [
                        {
                            "url": "http:\/\/t.co\/b5Oyx12qGG",
                            "expanded_url": "http:\/\/techcrunch.com",
                            "display_url": "techcrunch.com",
                            "indices": [0, 22]
                        }
                    ]
                },
                "description": {
                    "urls": []
                }
            },
            "protected": false,
            "followers_count": 5104212,
            "friends_count": 683,
            "listed_count": 97340,
            "created_at": "Wed Mar 07 01:27:09 +0000 2007",
            "favourites_count": 593,
            "utc_offset": -25200,
            "time_zone": "Pacific Time (US & Canada)",
            "geo_enabled": true,
            "verified": true,
            "statuses_count": 99688,
            "lang": "en",
            "contributors_enabled": false,
            "is_translator": false,
            "is_translation_enabled": true,
            "profile_background_color": "149500",
            "profile_background_image_url": "http:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_image_url_https": "https:\/\/pbs.twimg.com\/profile_background_images\/542331481398317056\/81Rbm71g.png",
            "profile_background_tile": false,
            "profile_image_url": "http:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_image_url_https": "https:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
            "profile_banner_url": "https:\/\/pbs.twimg.com\/profile_banners\/816653\/1431096993",
            "profile_link_color": "097000",
            "profile_sidebar_border_color": "FFFFFF",
            "profile_sidebar_fill_color": "DDFFCC",
            "profile_text_color": "222222",
            "profile_use_background_image": true,
            "default_profile": false,
            "default_profile_image": false,
            "following": true,
            "follow_request_sent": false,
            "notifications": false
        },
        "geo": null,
        "coordinates": null,
        "place": null,
        "contributors": null,
        "retweet_count": 23,
        "favorite_count": 7,
        "entities": {
            "hashtags": [],
            "symbols": [],
            "user_mentions": [
                {
                    "screen_name": "ron_miller",
                    "name": "Ron Miller",
                    "id": 5874452,
                    "id_str": "5874452",
                    "indices": [80, 91]
                }
            ],
            "urls": [
                {
                    "url": "http:\/\/t.co\/zOZoYwQJak",
                    "expanded_url": "http:\/\/tcrn.ch\/1EZCPYz",
                    "display_url": "tcrn.ch\/1EZCPYz",
                    "indices": [54, 76]
                }
            ]
        },
        "favorited": false,
        "retweeted": false,
        "possibly_sensitive": false,
        "lang": "en"
    }
]